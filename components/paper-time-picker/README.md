paper-time-picker
==========
Material design date picker component for `polymer`

See the [component page](http://bendavis78.github.io/paper-time-picker/) for full
documentation.
