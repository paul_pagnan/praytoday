# PrayToday #

PrayToday is an app focusing on supporting local parish communities, with emphasis upon the elderly. It provides a transport option for those who struggle to attend ceremonies at their local church. It is a transport service run by the individuals of a church, to assist the elderly in attending services throughout the week. People going to church log in details online through an app or PC. Those requiring transport send a request. If accepted, GPS is used to provide routes to pick up person and navigate to the church.


**Login Details:**

Passenger: passenger@praytoday.com

Driver: driver@praytoday.com

Password is irrelevant, enter anything here.


***DISCLAIMER:***

This app has been developed purely for the purposes of demonstration. Please do not attempt to use this app under any circumstances as it will not function properly and no support will be provided. The app has been developed as part of a university assignment.




## Screenshots ##
![Untitled image.png](https://bitbucket.org/repo/58qb8r/images/3324246127-Untitled%20image.png)

![Unnamed image.png](https://bitbucket.org/repo/58qb8r/images/3507448415-Unnamed%20image.png)