var marker; 

var d = document.URL;
var passengerId = d.split("=")[1];
var routeId;


var client = new WindowsAzure.MobileServiceClient(
    "https://praytodayapi.azure-mobile.net/",
    "hnvfOEClruYQlwoGquiFGiQCmKwFOR29"
);

var myLocation; 
var gps;

gmap.addEventListener('api-load', function(e) {
    //    document.querySelector('google-map-directions').map = this.map;
    gmap.dragEvents = true;
    setLocation(function() {
        myLocation = new google.maps.LatLng(gps.coords.latitude, gps.coords.longitude);
    });
    
    var passengersapi = client.getTable("passengers").lookup(passengerId).done(function(result) {
        routeId = result.route;
        
        console.log(routeId);
        if(routeId !== undefined) {
            getLocation().done(function( loc ) {
                driver = new google.maps.Marker({
                    position: new google.maps.LatLng(0, 0),
                    map: gmap.map,
                    icon: "/images/driver.png"
                });
                setInterval(function() { updateLocation(); }, 2000);
            });  
        }
        
        getLocation().done(function( loc ) {
            marker = new google.maps.Marker({
                position: loc,
                map: gmap.map,
                icon: "/images/location.png"
            });
        });
        setInterval(function() { updateMyLocation(); }, 2000);

        createRoute();
    });   
});

gmap.addEventListener('google-map-dragend', function(e) {
    $("#coords").text("Pin Location");
    checkLocation();
});

$("#location").click(function(e) {
    setLocation();
});

function checkLocation(callback) {
    if(gmap.longitude == gps.coords.longitude && gmap.latitude == gps.coords.latitude)
        $("#location").addClass("active");
    else
        $("#location").removeClass("active"); 

    callback;
}

function setLocation(callback) {
    if (navigator.geolocation) {
        if(gps) {
            setMap();
        }
        navigator.geolocation.getCurrentPosition(function(pos) {
            gps = pos; 
            setMap(function() { 
                callback;
            });
        }, function(error) {
            alert("Could not retrieve location.")
        });
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function setMap(callback) {
    gmap.latitude = gps.coords.latitude;
    gmap.longitude = gps.coords.longitude;   
    gmap.zoom = 16; 
    checkLocation(function() { callback; });
}


function createRoute() {
    var passengersapi = client.getTable("passengers").lookup( passengerId ).done(function(result) {
        setTimeout(function() {
            createMapRoute(result.pickup, result.destination, "mainmap");
        }, 500); 
    });
}

function updateMyLocation() {
    getLocation().done(function(loc) {
        marker.setPosition(loc);
    });
}

function updateLocation() {    
    var route = client.getTable("routes").lookup(routeId).done(function(result) {
        var driverloc = new google.maps.LatLng(result.currentlat, result.currentlong);
        driver.setPosition(driverloc);
        console.log(driverloc);
        console.log("Got Location");  
    });
}

