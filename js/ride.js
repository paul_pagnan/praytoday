

var marker; 

var d = document.URL;
var routeId = d.split("=")[1];


var client = new WindowsAzure.MobileServiceClient(
    "https://praytodayapi.azure-mobile.net/",
    "hnvfOEClruYQlwoGquiFGiQCmKwFOR29"
);

var myLocation; 
var gps;

gmap.addEventListener('api-load', function(e) {
    //    document.querySelector('google-map-directions').map = this.map;
    gmap.dragEvents = true;
    setLocation(function() {
        myLocation = new google.maps.LatLng(gps.coords.latitude, gps.coords.longitude);
    });
    createRoute();
    
    getLocation().done(function( loc ) {
        marker = new google.maps.Marker({
            position: loc,
            map: gmap.map,
            icon: "/images/location.png"
        });
    });
    
    setInterval(function() { updateLocation(); }, 2000);
});

gmap.addEventListener('google-map-dragend', function(e) {
    $("#coords").text("Pin Location");
    checkLocation();
});

$("#location").click(function(e) {
    setLocation();
});

function checkLocation(callback) {
    if(gmap.longitude == gps.coords.longitude && gmap.latitude == gps.coords.latitude)
        $("#location").addClass("active");
    else
        $("#location").removeClass("active"); 

    callback;
}

function setLocation(callback) {
    if (navigator.geolocation) {
        if(gps) {
            setMap();
        }
        navigator.geolocation.getCurrentPosition(function(pos) {
            gps = pos; 
            setMap(function() { 
                callback;
            });
        }, function(error) {
            alert("Could not retrieve location.")
        });
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function setMap(callback) {
    
    gmap.latitude = gps.coords.latitude;
    gmap.longitude = gps.coords.longitude;   
    gmap.zoom = 16; 
    checkLocation(function() { callback; });
}


function createRoute() {
    var route = client.getTable("passengers").where({ 
        route: routeId, 
        status: "accepted" 
    }).read().done(function (passengers) {
        for (var i = 0; i < passengers.length; i++) {
            client.getTable("passengers").update({ id: passengers[i].id, status: "on route" });
        }
    });
    
    
    getLocation().done(function( addr ) {
        var passengersapi = client.getTable("passengers").where({ route: routeId }).read().done(function(presults) {
            var waypoints = [];
            if(presults.length > 0) {
                for(var j = 0; j < presults.length; j++) {
                    waypoints.push({location: presults[j].pickup, stopover: true});    
                }
                
                setTimeout(function() {
                    createMapRoute(addr, presults[0].destination, "mainmap", waypoints);
                }, 500); 
            } else {
                var routes = client.getTable("routes").lookup(routeId).done(function(result) {
                    setTimeout(function() {
                        createMapRoute(addr, result.destination, "mainmap");
                    }, 200); 
                });
            }
        });
        $(".spinner").hide(); 
    });   
}

function updateLocation() {    
    getLocation().done(function( loc ) {
        marker.setPosition(loc);
        
        var route = client.getTable("routes").update({ id: routeId, currentlat: loc.A, currentlong: loc.F }).done(function() {
          console.log("Updated Location");  
        });
    });
}

