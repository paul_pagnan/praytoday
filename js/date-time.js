
//Date Pickers
document.addEventListener('DOMContentLoaded', function() {
    var scope = document.querySelector('#time');
    scope.updateTime = function(hour, minute) {
        scope.hour = hour;
        scope.minute = minute;
        scope.time = (hour % 12 || 12) + ':' + ('0' + minute).substr(-2);
        scope.time += ' ' + ['pm', 'am'][+(hour < 12)];
    }
    scope.timeSelected = function() {
        var picker = document.querySelector('html /deep/ #picker');
        scope.updateTime(picker.hour, picker.minute);
    },
        scope.showDialog = function() {
        this.$.timedialog.toggle();
    };
    scope.updateTime(15, 20);     
});


document.addEventListener('DOMContentLoaded', function() {
    var dateScope = document.querySelector('#date');
    
    dateScope.updateDate = function() {
        var date = new Date();
        dateScope.min = americanDate(date);
        dateScope.date = date.toLocaleDateString();
    }
    dateScope.showDialog = function() {
        document.querySelectorAll('paper-date-picker-dialog')[0].open();
        setTimeout(function() {
            document.querySelectorAll('paper-date-picker-dialog')[0].immediateDate = new Date();
        }, 200);
    }
    dateScope.updateDate(); 
});

function americanDate(date) {
    return Date.parse((date.getMonth() + 1) + "/" + (date.getDate()) + "/" + date.getFullYear());   
}


document.addEventListener('value-changed', function() {
    var dateinput = document.querySelectorAll('paper-date-picker-dialog')[0].value;
    var strArr = dateinput.toString().split(" ");
    var str = strArr[0] + ", ";
    for(var i = 1; i < strArr.length; i++) {
        str += strArr[i] + " ";   
    }
    var date = new Date(str.split("(")[0]);
    $("#date-input").val(date.toLocaleDateString());
});