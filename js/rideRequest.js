var gps;
navigator.geolocation.getCurrentPosition(function(pos) {
    gps = pos; 
});
var gmap = document.querySelector("google-map");

var mapDraw;

var client = new WindowsAzure.MobileServiceClient(
    "https://praytodayapi.azure-mobile.net/",
    "hnvfOEClruYQlwoGquiFGiQCmKwFOR29"
);

gmap.addEventListener('api-load', function(e) {

    getLocation().done(function( addr ) {
        var ridesapi = client.getTable("passengers").where({ username: "Liam" }).orderBy("arriveby").read().done(function(results) {                                                       
            var rides = [];
            var ridesElem = document.querySelector('map-item-list');
            for(var i = 0; i < results.length; i++) {
                var current = results[i];
                var ride = {
                    id: current.id,
                    name: current.church,
                    pickup: current.pickup,
                    destination: current.destination,
                    status: current.status,
                    class: getClass(current.status),
                    datetime: current.arriveby,
                    map: "map-" + i
                };
                rides.push(ride);
            }
            
            ridesElem.rides = rides;
            $(".spinner").hide();
            setTimeout(function() {
                for(var i = 0; i < results.length; i++) {
                    createMapRoute(rides[i].pickup, rides[i].destination, "map-" + i);
                }
            }, 700);  
        });
    });    
});


function cancel(rideId) {
    var ridesapi = client.getTable("passengers").del({ id: rideId }).done(function() {     
        document.location.replace(document.URL);
    });
}


function getClass(status) {
    switch(status) {
        case "pending": return "warning"; 
        case "cancelled": return "error";
        default: return "success"; 
    }
}