var gmap = document.querySelector('#mainmap');
var numLocations = 3;

function createMapRoute(start,end, id, waypoints) {
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();

    var map = document.querySelector("html /deep/ #" + id).map;    
    directionsDisplay.setMap(map);
    calcRoute(start, end, directionsService, directionsDisplay, waypoints);
}


function calcRoute(start, end, directionsService, directionsDisplay, waypointsarr) {
    if(waypointsarr === undefined) {
       waypointsarr = [];
    }

    var request = {
        origin:start,
        destination:end,
        waypoints: waypointsarr,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });
}

gmap.addEventListener('api-load', function(e) {
    var input = document.getElementById('search');
    if(input !== undefined) {
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
    }
});

$("#search-button").click(function() {
    $("#search").toggle('fast', function() {
        updateDrawer();
    });
});


function nearbyChurches() {  
    var request = {
        location: myLocation,
        radius: '10000',
        types: ['church', 'place_of_worship']
    };   
    
    var map = gmap;
    if(document.querySelector('#mapsapi') != null)
        map = document.querySelector('#mapsapi'); 
        
    var mapsapi = new google.maps.places.PlacesService(map);
    mapsapi.nearbySearch(request, searchCallback);
}

var destinations = [];
var churches = [];
var formattedChurches = [];
function searchCallback(results, status) {
    if(status == "OK") {       
        var arr;
        for(var i = 0; i < numLocations; i++) {
            var church = results[i];
            churches.push(church);
            destinations.push(new google.maps.LatLng(church.geometry.location.A, church.geometry.location.F));
        }
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [myLocation, myLocation, myLocation],
                destinations: [destinations[0], destinations[1], destinations[2]],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function(response, status) {
                if (status != google.maps.DistanceMatrixStatus.OK) {
                    alert('Error was: ' + status);
                } else {
                    var origins = response.originAddresses;
                    var results = response.rows[0].elements;
                    for (var j = 0; j < results.length; j++) {
                        var church = churches[j];
                        formattedChurches.push({
                            name: church.name,
                            address: church.vicinity,
                            distance: results[j].distance.text
                        });  
                    }
                    var locations = document.querySelector('location-list');
                    locations.locations = formattedChurches;
                    setTimeout(function() { updateDrawerMinimised();  clickEventListener(); }, 300);
                }
            });
    }
}

function clickEventListener() {
    $("html /deep/ location-list-item").click(function(e) {
        $("html /deep/ location-list-item.active").removeClass("active");
        $(this).addClass("active");
    });
    
    $('html /deep/ location-list-item').on('touchstart click', function(){ 
        $("html /deep/ location-list-item.active").removeClass("active");
        $(this).addClass("active");
    });
}

function getLocation() {
    var dfd = $.Deferred();

    navigator.geolocation.getCurrentPosition(function(pos) {
        var myLocation = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
        dfd.resolve(myLocation);
    });
    return dfd.promise();
}

function getAddress(location) {
    var dfd = $.Deferred();
    var geocoder = new google.maps.Geocoder();
    if(location === undefined) {
        navigator.geolocation.getCurrentPosition(function(pos) {
            var myLocation = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            geocoder.geocode({'latLng': myLocation}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        dfd.resolve(results[0].formatted_address);
                    }
                }
            });
        });
    }
    else {
        geocoder.geocode({'latLng': location}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) 
                    dfd.resolve(results[0].formatted_address);
            }
        });
    }
    return dfd.promise();
}

