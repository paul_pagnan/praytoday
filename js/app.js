var gmap = document.querySelector('google-map');
var gmapMarker = document.querySelector('google-map-marker');

var client = new WindowsAzure.MobileServiceClient(
    "https://praytodayapi.azure-mobile.net/",
    "hnvfOEClruYQlwoGquiFGiQCmKwFOR29"
);

var geocoder;
var myLocation; 
var latlng;
var gps;

gmap.addEventListener('api-load', function(e) {
    //    document.querySelector('google-map-directions').map = this.map;
    gmap.dragEvents = true;
    geocoder = new google.maps.Geocoder();
    setLocation(function() {
        myLocation = new google.maps.LatLng(gps.coords.latitude, gps.coords.longitude);
        codeLatLng(); 
        nearbyChurches();
    });
});

gmap.addEventListener('google-map-dragend', function(e) {
    $("#coords").text("Pin Location");
    checkLocation();
    codeLatLng();
});

$("#location").click(function(e) {
    setLocation();
});


function codeLatLng() {
    var latlng = new google.maps.LatLng(gmap.latitude, gmap.longitude);
    var fallback = "(" + gmap.longitude + ", " + gmap.latitude + ")";
    var addr;
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                $("#coords").text(results[0].address_components[0].short_name + " " + results[0].address_components[1].short_name);
            }
            else {
                $("#coords").text(fallback);
            }
        }
    });
}

function checkLocation(callback) {
    if(gmap.longitude == gps.coords.longitude && gmap.latitude == gps.coords.latitude)
        $("#location").addClass("active");
    else
        $("#location").removeClass("active"); 

    callback();
}

function setLocation(callback) {
    if (navigator.geolocation) {
        if(gps) {
            setMap();
        }
        navigator.geolocation.getCurrentPosition(function(pos) {
            gps = pos; 
            setMap(function() { 
                callback();
            });
        }, function(error) {
            alert("Could not retrieve location.")
        });
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function request() {
    var time = document.querySelector("#time-input").value;
    var date = document.querySelector("#date-input").value;
    var timeDate = date + " " + time;
    var address = $("html /deep/ location-list-item.active address").text();
    var churchName = $("html /deep/ location-list-item.active name").text();
    
    var mapLoc = new google.maps.LatLng(gmap.latitude, gmap.longitude);
    
    getAddress(mapLoc).done(function( addr ) {
        console.log(addr);
        
        if(time == "" || date == "" || address == "") 
            alert("Please complete all fields");
        else {
            var item = { username: "Liam", church: churchName, destination: address, arriveby: timeDate, status: "pending", pickup: addr, route: null };
            client.getTable("passengers").insert(item).done(function() {;
                document.location.href = "requestedRides.html";
            });
        }   
    });
}

function setMap(callback) {
    gmap.latitude = gps.coords.latitude;
    gmap.longitude = gps.coords.longitude;   
    gmap.zoom = 16; 

    gmapMarker.longitude = gps.coords.longitude;
    gmapMarker.latitude = gps.coords.latitude;

    checkLocation(function() { callback() });
}
