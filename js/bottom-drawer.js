var deltaX = 0;
var dimAmount = 0.5;
var dimFactor = 1.5;
var speed = 300;

var min = $(window).height() - $("#bottom-drawer").height() - 50;
var max = $(window).height() - 60; 

$(window).resize(function() {
    min = $(window).height() - $("#bottom-drawer").height() - 50;
    max = $(window).height() - 60; 
    $( "#bottom-drawer" ).animate({
        top: max,
    }, 0, "swing");
});


function updateDrawer() {
    min = $(window).height() - $("#bottom-drawer").height() - 50;
    $( "#bottom-drawer" ).animate({
        top: min,
    }, 0, "swing");   
}

function updateDrawerMinimised() {
    min = $(window).height() - $("#bottom-drawer").height() - 50;
    max = $(window).height() - 60;
    $( "#bottom-drawer" ).animate({
        top: max,
    }, 0, "swing");   
}

$(document).ready(function() {
    showSmallDrawer()
});

var lastY = 0;
var Y = 0;
$('#bottom-drawer').draggable({ axis: "y",
   drag: function(event, ui) {
       if(ui.position.top > max)
           ui.position.top = max;
       else if (ui.position.top < min) 
           ui.position.top = min;

       changeIcons(ui.position.top);
       lastY = Y;
       Y = ui.position.top;
   },
   stop: function(event, ui) {
       if(ui.position.top < max && ui.position.top > min) {
           if( Y > lastY )
               showSmallDrawer(); //GOING DOWN
           else
               showDrawer(); //GOING UP
       }
       changeIcons(ui.position.top);
   }                          
});

$('#bottom-drawer').bind('drag',function(e, delta){
    if(Y != 0 && lastY != 0)
        deltaX += (lastY - Y);

    if(deltaX > 0) {
        if(!$("#dimmer").hasClass("dimmer"))
            $("#dimmer").addClass("dimmer"); 
        var opac = Math.min((deltaX / $("#bottom-drawer").height()) / dimFactor, dimAmount);
        $(".dimmer").css("opacity", opac);
    }
    else {
        $("#dimmer").removeClass("dimmer");  
    }
});

$("#toggle").click(function() {
    $("#dimmer").addClass("dimmer"); 
    showDrawer();
});

function changeIcons(top) {
    if(top == min) {
        $("#bottom-drawer .caret").attr("icon", "arrow-drop-down");
    }
    else if (top == max) { 
        $("#bottom-drawer .caret").attr("icon", "arrow-drop-up");
    }
}

function showSmallDrawer() {
    $( "#bottom-drawer" ).animate({
        top: max,
    }, speed, "swing"); 

    $( ".dimmer" ).animate({
        opacity: 0,
    }, speed, "swing", function() {
        $("#dimmer").removeClass("dimmer");
        changeIcons( $( "#bottom-drawer" ).offset().top );
    });
}

function showDrawer() {
    $( "#bottom-drawer" ).animate({
        top: min,
    }, speed, "swing"); 

    $( ".dimmer" ).animate({
        opacity: dimAmount,
    }, speed, "swing", function() {
        changeIcons( $( "#bottom-drawer" ).offset().top );
    });
}
