var gps;
navigator.geolocation.getCurrentPosition(function(pos) {
    gps = pos; 
});
var gmap = document.querySelector("google-map");

var client = new WindowsAzure.MobileServiceClient(
    "https://praytodayapi.azure-mobile.net/",
    "hnvfOEClruYQlwoGquiFGiQCmKwFOR29"
);

var mapDraw;

gmap.addEventListener('api-load', function(e) {
    var routes = client.getTable("routes").read().done(function(routeresults) {
        
        var churches = [];
        for(var i = (routeresults.length - 1); i >= 0; i--) {
            churches.push(routeresults[i]);
        }
        
        getLocation().done(function( addr ) {
            var count = 0;
            var ridesapi = client.getTable("passengers").where({ status: "pending" }).read().done(function(results) { 
                
                var rides = [];
                var ridesElem = document.querySelector('map-item-list-request');
                for(var i = 0; i < results.length; i++) {                    
                    var current = results[i];
       
                    if(any(current.church, churches)) {
                        var ride = {
                            id: current.id,
                            routeId: match(current.church, churches),
                            name: current.church,
                            pickup: current.pickup,
                            destination: current.destination,
                            datetime: current.arriveby,
                            user: "/images/oldm8.jpg",
                            map: "map-" + count++
                        };
                        rides.push(ride);
                    }
                }

                ridesElem.rides = rides;
                $(".spinner").hide();
                setTimeout(function() {
                    for(var i = 0; i < rides.length; i++) {
                        createMapRoute(rides[i].pickup, rides[i].destination, "map-" + i)
                    }
                }, 700);  
            });
        });
    });
});



function any(item, array) {
    for(var i = 0; i < array.length; i++) {
        if(item == array[i].church)
            return true;
    }
    return false;
}
                    
function match(item, array) {
    for(var i = 0; i < array.length; i++) {
        if(item == array[i].church)
            return array[i].id;
    }
    return -1;
}

function accept(rideId, routeId) {
    var ridesapi = client.getTable("passengers").update({id: rideId, status: "accepted", route: routeId}).done(function() { 
        document.location.replace(document.URL);
    });
}