var gps;

var gmap = document.querySelector('#mainmap');

var client = new WindowsAzure.MobileServiceClient(
    "https://praytodayapi.azure-mobile.net/",
    "hnvfOEClruYQlwoGquiFGiQCmKwFOR29"
);

var mapDraw;


gmap.addEventListener('api-load', function(e) {
    var count = 0; 

    getLocation().done(function( addr ) {
        var ridesapi = client.getTable("routes").read().done(function(results) {
            var rides = [];
            var passengers = [];
            var completed = false;
            
            var ridesElem = document.querySelector('map-item-list-route');
            
            for(var i = 0; i < results.length; i++) {
                var current = results[i];                        
                var ride = {
                    id: current.id,
                    name: current.church,
                    pickup: current.destination,
                    passengers: 0,
                    datetime: current.arriveby,
                    map: "map-" + i
                };
                rides.push(ride);
                ridesElem.rides = rides;
                
                var passengersapi = client.getTable("passengers").where({ route: current.id}).read().done(function(presults) {
                    var waypoints = [];
                    if(presults.length > 0) {
                        var index = getIndex(rides, presults[0].route);
                        rides[index].passengers = presults.length;
                        for(var j = 0; j < presults.length; j++) {
                            waypoints.push({location: presults[j].pickup, stopover: true});    
                        }
                   
                        setTimeout(function() {
                            createMapRoute(addr, rides[index].pickup, "map-" + index, waypoints)
                        }, 450); 
                    }
                });
            }
            
            setTimeout(function() {
                for(var i = 0; i < rides.length; i++) {
                    if(rides[i].passengers == 0)
                        createMapRoute(addr, rides[i].pickup, "map-" + i)
                }
            }, 750); 
            
            $(".spinner").hide(); 
        }); 
    });
});


function getIndex(arr, item) {
    for(var i = 0; i < arr.length; i++) {
        if(item == arr[i].id) 
            return i;
    }
    return -1;        
}
   
function cancel(routeId) {
    var ridesapi = client.getTable("routes").del({ id: routeId }).done(function() {   
        var passengers = client.getTable("passengers").where({ route: routeId }).read().done(function(results) { 
            for(var i = 0; i < results.length; i++) {
                var updatePassenger = client.getTable("passengers").update({ id: results[i].id, status: "cancelled" }).done(function() { 
                    document.location.replace(document.URL);
                });
            }
            if(results.length == 0)
                document.location.replace(document.URL);
        });
    });
}

